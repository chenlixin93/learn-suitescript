define(["N/search", "N/ui/serverWidget", "N/url", "N/log"], function (s, ui, url, log) {

    /**
     * Example Suitelet: Display Search Results in a List
     *
     * @exports suitelet-results
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @requires N/search
     * @requires N/ui/serverWidget
     * @requires N/url
     * @requires N/log
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType Suitelet
     */
    var exports = {};

    /**
     * <code>onRequest</code> event handler
     *
     * @governance 0
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @return {void}
     *
     * @static
     * @function onRequest
     */
    function onRequest(context) {
        log.audit({title: "Request received."});

        context.response.writePage({
            pageObject: renderList(translate(findCases()))
        });
    }

    function renderList(results) {
        log.audit({title: "Rendering list..."});

        var list = ui.createList({title: "High Priority Cases"});

        list.clientScriptModulePath = "./suitelet-results-cl.js";

        list.addButton({
            id: "custpage_btn_nextcase",
            label: "Go to Next Case",
            functionName: "goToNextCase"
        });

        list.addPageLink({
            type: ui.FormPageLinkType.CROSSLINK,
            title: "List All Cases",
            url: "/app/crm/support/caselist.nl"
        });
        list.addPageLink({
            type: ui.FormPageLinkType.CROSSLINK,
            title: "Search Cases",
            url: "/app/common/search/search.nl?searchtype=Case"
        });
        list.addPageLink({
            type: ui.FormPageLinkType.CROSSLINK,
            title: "Learn JavaScript!",
            url: "https://developer.mozilla.org/en-US/"
        });

        list.addColumn({
            id: "recordurl",
            type: ui.FieldType.URL,
            label: "Case Number"
        }).setURL({
            url: getBaseUrl()
        }).addParamToURL({
            param: "id",
            value: "internalid",
            dynamic: true
        }).addParamToURL({
            param: "cf",
            value: -100
        });
        list.addColumn({
            id: "status",
            type: ui.FieldType.TEXT,
            label: "Status"
        });
        list.addColumn({
            id: "priority",
            type: ui.FieldType.TEXT,
            label: "Priority"
        });
        list.addColumn({
            id: "title",
            type: ui.FieldType.TEXT,
            label: "Subject"
        });
        list.addColumn({
            id: "createddate",
            type: ui.FieldType.DATE,
            label: "Date Created"
        });

        list.addRows({rows: results});

        return list;
    }

    function findCases() {
        log.audit({title: "Finding Cases..."});

        return s.create({
            type: s.Type.SUPPORT_CASE,
            filters: [
                ["status", s.Operator.NONEOF, ["5"]], "and", // Not Closed
                ["priority", s.Operator.ANYOF, ["1"]] // High Priority
            ],
            columns: [
                "casenumber",
                "status",
                "priority",
                "title",
                {
                    name: "createddate",
                    sort: s.Sort.ASC
                }
            ]
        }).run().getRange({start: 0, end: 20});
    }

    function resultToObject(result) {
        return {
            recordurl: result.getValue({name: "casenumber"}),
            status: result.getText({name: "status"}),
            priority: result.getText({name: "priority"}),
            title: result.getValue({name: "title"}),
            createddate: result.getValue({name: "createddate"}),
            internalid: result.id
        };
    }

    function translate(results) {
        log.audit({title: "Translating results..."});
        return results.map(resultToObject);
    }

    function getBaseUrl() {
        return url.resolveRecord({
            recordType: s.Type.SUPPORT_CASE
        });
    }

    exports.onRequest = onRequest;
    return exports;
});
