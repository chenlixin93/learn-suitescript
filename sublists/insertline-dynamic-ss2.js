// Inserting a line item at the beginning of a sublist in Dynamic Mode in SuiteScript 2.0 ...

require(["N/record"], function (r) {
    var rec = r.create({
        "type": r.Type.SALES_ORDER,
        "isDynamic": true
    });

    // Set relevant body fields ...

    // Insert line item 456 with quantity 10 at the beginning of the Items sublist
    rec.insertLine({"sublistId": "item", "line": 0});
    rec.selectLine({"sublistId": "item", "line": 0});
    rec.setCurrentSublistValue({"sublistId": "item", "fieldId": "item", "value": 456});
    rec.setCurrentSublistValue({"sublistId": "item", "fieldId": "quantity", "value": 10});
    rec.commitLine({"sublistId": "item"});

    // Insert line item 777 with quantity 3 before the last line of the Items sublist
    var lastIndex = rec.getLineCount({"sublistId": "item"}) - 1; // 2.0 sublists have 0-based index
    rec.insertLine({"sublistId": "item", "line": lastIndex}); // The last line will now actually be at lastIndex + 1
    rec.selectLine({"sublistId": "item", "line": lastIndex});
    rec.setCurrentSublistValue({"sublistId": "item", "fieldId": "item", "value": 777});
    rec.setCurrentSublistValue({"sublistId": "item", "fieldId": "quantity", "value": 3});
    rec.commitLine({"sublistId": "item"});

    rec.save();
});
