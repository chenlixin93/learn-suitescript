// How many lines in a sublist in SuiteScript 2.0...

require(["N/record"], function (r) {
    var rec = r.load({
        "type": r.Type.SALES_ORDER,
        "id": 123
    });

    // How many lines are on the Items sublist?
    var itemCount = rec.getLineCount({"sublistId": "item"});
});
