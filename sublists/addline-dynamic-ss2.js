// Adding a line item to the end of a sublist in Dynamic Mode in SuiteScript 2.0...

require(["N/record"], function (r) {
    var rec = r.create({
        "type": r.Type.SALES_ORDER,
        "isDynamic": true
    });

    // Set relevant body fields ...

    // Add line item 456 with quantity 10 at the end of the Items sublist
    var itemCount = rec.selectNewLine({"sublistId": "item"});
    rec.setCurrentSublistValue({"sublistId": "item", "fieldId": "item", "value": 456});
    rec.setCurrentSublistValue({"sublistId": "item", "fieldId": "quantity", "value": 10});
    rec.commitLine({"sublistId": "item"});

    rec.save();
});
