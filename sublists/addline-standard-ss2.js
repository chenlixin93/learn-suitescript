// Adding a line item to the end of a sublist in Standard Mode in SuiteScript 2.0...

require(["N/record"], function (r) {
    var rec = r.create({
        "type": r.Type.SALES_ORDER,
        "isDynamic": false
    });

    // Set relevant body fields ...

    // Add line item 456 with quantity 10 at the end of the Items sublist
    var itemCount = rec.getLineCount({"sublistId": "item"});
    rec.setSublistValue({"sublistId": "item", "fieldId": "item", "value": 456, "line": itemCount});
    rec.setSublistValue({"sublistId": "item", "fieldId": "quantity", "value": 10, "line": itemCount});

    rec.save();
});
