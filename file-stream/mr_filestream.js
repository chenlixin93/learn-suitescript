define(["N/file"], function (f) {

    /**
     * Module Description...
     *
     * @exports XXX
     *
     * @copyright 2018 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType MapReduceScript
     */
    var exports = {};

    /**
     * <code>getInputData</code> event handler
     *
     * @governance XXX
     *
     * @return {*[]|Object|Search|ObjectRef} Data that will be used as input for
     *         the subsequent <code>map</code> or <code>reduce</code>
     *
     * @static
     * @function getInputData
     */
    function getInputData() {
        return f.load({id: "7268"});
    }


    /**
     * <code>reduce</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {ReduceContext} Data collection containing the key/value pairs to
     *          process through the reduce stage
     *
     * @return {void}
     *
     * @static
     * @function reduce
     */
    function reduce(context) {
        log.debug({title: "Data:", details: context.values});
    }


    exports.getInputData = getInputData;
    exports.reduce = reduce;
    return exports;
});
