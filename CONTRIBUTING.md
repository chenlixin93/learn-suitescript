Have an example to add? Great! Here's how you can help:

1. Clone the repo

    `git clone git@gitlab.com:stoicsoftware/learn-suitescript.git`
    
1. Create a new feature branch for your example

    `git checkout -b feature/my-example-name`
    
1. Create and commit your example
1. Submit a Merge Request for your example's feature branch 
